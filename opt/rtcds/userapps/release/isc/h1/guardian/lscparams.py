#
# $Id$
# $HeadURL$

import numpy


# If you change these input powers, you *must* also reload the LASER_PWR guardian
# before doing an initial alignment.
# The LASER_PWR guardian creates fast states based on these numbers, and ALIGN_IFO guardian tries to
# go to those fast states.  So, ALIGN_IFO guardian will fail if the other guardian LASER_PWR
# states don't exist yet.
# Also, the IMC_LOCK guardian depends on the NLN number, so make sure to reload IMC_LOCK if you change the NLN number.
input_power = {'PRXY': 2,
               'MICH': 10,
               'SRXY': 10,
               'POWER_10W':  10,
               'POWER_20W':  20,
               'NLN':38}
# normally MICH=10, SRXY=10, but lowering for time when HAM6 in air

### ISS Final Gain Value
ISS_acquisition_gain = 0
if input_power['NLN'] > 35:
	ISS_FinalGain = 3 #for 40 W
elif input_power['NLN'] > 30:
	ISS_FinalGain = 5 #for 35W alog 47607
else:
	ISS_FinalGain = 7 #for 30W


### CARM Gain sliders initial values for the beginning of locking
# IMC
IMC_FASTGAIN_PowerUp_Scaler = 6 # CC: Value we want on IMC_FASTGAIN when at 2 watts, IMC_FASTGAIN is controlled by ISC_library.IMC_power_adjust()
IMC_IN1_gain = -1 # -20dB   JCD 7May2019
IMC_MCL_gain = 1
# COMM
COMM_IMC_IN2GAIN = -12 # CC: Jan 23, 2019, defining new initial COMM gain here
COMM_LSC_IN2GAIN = -32
COMM_LSC_FASTGAIN = 7
COMM_HANDOFF_PART2_LSC_IN2GAIN = 17
# PREP_TR_CARM
#LSC_REFLBIAS_GAIN_PREP_TR_CARM = 50.4   #moved to carm offset dict
LSC_REFL_SUM_A_IN1GAIN_PREP_TR_CARM = 0
LSC_REFL_SERVO_IN1GAIN_PREP_TR_CARM = -16
# START_TR_CARM
ALS_C_REFL_DC_BIAS_GAIN_START_TR_CARM = 24
# CARM_TO_TR
# CARM_150_PICOMETERS
#ALS_C_REFL_DC_BIAS_GAIN_CARM_150_PICOMETERS = 39  #moved to carm offset dict
# CARM_OFFSET_REDUCTION
LSC_REFLBIAS_GAIN_CARM_OFFSET_REDUCTION = 86
# CARM_TO_REFL
LSC_TR_REFLAIR9_CARM_TO_REFL = -0.8

#these are parameters that are used in CARM offset reduction, collecting them here so that when we have a bad recycling gain we can preseve the old settings but use some temporary ones
'''carm_offset ={'TR_CARM_offsets':
                {'RF_DARM': -5,
                'DARM_boost':-20,
                'carmH0_OFSref':-40
                'als_c_comm_vco_freq': -200,
                'carm_to_tr': -0.8,},
              'gains':
                {'LSC_REFLBIAS_PREP_TR_CARM':50.4,
                 'ALS-C_REFLBIAS_150PM':39,
                 'ALS_C_DIFF_PLL_servo': 0.01,
                 'RF_DAMR_intrix':-1e-6,
                 'LSC_REFLBIAS_CARM_OFFSET_REDUCTION':86},
             }'''
#October 2019 terrible recycling gain relocking
carm_offset ={'TR_CARM_offsets':
                {'RF_DARM': -3,
                 'DARM_boost': -12,
                 'carmH0_OFSref': -40,
                 'als_c_comm_vco_freq': -200,
                 'carm_to_tr': -0.8,},
              'gains':
                {'LSC_REFLBIAS_PREP_TR_CARM':50.4,
                 'ALS-C_REFLBIAS_150PM':39,
                 'ALS_C_DIFF_PLL_servo': 0.01,
                 'RF_DARM_intrix':-1e-6, 
                 'LSC_REFLBIAS_CARM_OFFSET_REDUCTION':86},
             }

# parameters related to CARM (offset reduction) sequence
carmHO_OFSref      =-40   # reference OFFSET for checking REFL_DSC
carmHO_PrefFraction= 0.57 # from alog 43346, REFL_DC fraction at OFFSET = -40 (arm transmission 800)
carmHO_OFSideal    =-50   # was -54 this is the target corresponding to about 85% of arm buildup for handoff to RF.

# Defining omc_sign for DARM_OFFSET state
omc_sign = 1

# Modulation depths (in slider units)
mod_depth = {
    '9':  23.4,
    '45': 27.0,
}

gate_valve_flag = False # False means arm gate valves open

# Which ESD to use for ALS locking?
ALS_ESD = 'X'

ETMX_ESD_LOWNOISE_BIAS = -1 # -1 means -400 Volts


# When transitioning lownoise ESD, stay on split actuator or go all the way back to ETMX?
use_EX_L1L2 = True


gain = {'MICH_DARK':
                {'ACQUIRE':4000,
                 'LOCKED': 8000}, # was 2000 for ACQ, 1000 for LOCKED, changed by TVo and Hang
        'MICH_BRIGHT':
                {'ACQUIRE':-4000,
                 'LOCKED': -8000}, #was 2000 for acq and 1000 for locked, changed GLM Sept 13 2018
        'PRXY':-500000, # Was changed sometime btwn April 5-23 2019 from -3200 to -525000.  Lowering to -520000 to not rail PRM JCD 5Aug2019
        'SRXY':78000, # changed from 10000, SED CRC July 22 2018, back to 10000 SB Aug 25 2018
        'XARM_IR':0.04,
        'YARM_IR':0.04,
        'DRMI_MICH':{'ACQUIRE':1.1, # 2018-11-28 TVo, SED from 2.8 as a guess from estimating the PRMI OLTF
                     'NO_ARMS':3.0, # 2018-11-26 TVo changed to 3.0 to match the OLG
                     'W_ALS': 2.8, # was 1.4, increased July 28 2018, was 1.4 - now in FM2
                     '3F_135': 1.115, # 2.23 #changed from 1.25 SED CRC June 21 2018
                     '3F_27I': -0.674, # -1.348
                     '1F_45': 1.824, # multiplied for whitneing gain change alog 44420
                     '1F_9': 0.0}, # removed 2018-10-17 # 0.00643 # 0.009 # added to remove PRCL to MICH coupling GV 2018-08-07
        'DRMI_PRCL':{'ACQUIRE':12, # was 12 # was 16, changed 10Sept2018 JCD
                     'NO_ARMS':18,
                     'W_ALS': 8,  # was 8, changed July 28 2018
                     '3F_27':  -1.22, # was -2.43
                     '1F_9': 0.037},  #0.035
        'DRMI_SRCL':{'ACQUIRE':-30,
                     'NO_ARMS':-33,
                     'W_ALS':  -18,
                     '3F_27':  2.124,
                     '3F_135':  1.7,
                     '1F_9':  0.024, # tuned 190110, soz RXA, # tuned 181224, RXA - stop changing this Stefan !!
                     '1F_45': 1.856}, #mulitppiled by 32 for whitening gain change alog 44420
        'PRMI_MICH':{'W_ALS':3.2,   # Changed from 2.8, measured OLG TVo, SED 20181128
                     'NO_ARMS':2.8}, # changed from 2.8, SED CRC June 24 2018
        'PRMI_PRCL':{'W_ALS':5,       # 12 is too high.  JCD 10Sept2018
                     'NO_ARMS':10, # Changed from 16, measured OLG TVo, SED 20181128
                     'CARRIER':-30},
        'MICHFF':  1,
        'SRCLFF1':  1,
        'SRCLFF2':  0,
        'PRCLFF': 1,
        'ALS_ASC_DOF3': {'X': 0.1, 'Y': 0.03},
        }

asc_gains = {'DHARD': {'P':{'DHARD_WFS_initial':-1,
                            'DHARD_WFS_final':-10,
                            'RESONANCE':-30,
                            'ENGAGE_ASC_FOR_FULL_IFO':-50,
			    'MOVE_SPOTS': -40,
                            'LOWNOISE_ASC':-30,              # Seems a bit too high with -42, so back to -30 to reduce 4.5 Hz buzzing. JCD 14Mar2019.[Was -30, with new 1Hz resG need -42 JCD 6Feb2019]
                            },
                        'Y':{'DHARD_WFS_initial':-1,
                            'DHARD_WFS_final':-15,
                            'RESONANCE':-40,
                            'ENGAGE_ASC_FOR_FULL_IFO':-60,
                            'LOWNOISE_ASC':-40,
                            },
                       },#close DHARD
                'CHARD':{'P':{'ENGAGE_ASC_FOR_FULL_IFO':2,
                              'LOWNOISE_ASC':0.6,
                             },
                        'Y':{'ENGAGE_ASC_FOR_FULL_IFO_initial':0.5,
                              'ENGAGE_ASC_FOR_FULL_IFO_increase':2.5,
                              'ENGAGE_ASC_FOR_FULL_IFO_final':25,
                             },
                        },#close CHARD
                'CSOFT':{'P':{'ENGAGE_SOFT_LOOPS':15,
                              'LOWNOISE_ASC':20, #alog 46975
                             },#close csoft p
                          'Y':{'ENGAGE_SOFT_LOOPS_initial':2.5, #CSOFT Y is not used, but there is an if statement and a flag
                               'ENGAGE_SOFT_LOOPS_final':3, #probably unnecessary to make such a small gain adjustment
                              },#close csoft y
                            },#close csoft
                'DSOFT':{'P':{'ENGAGE_SOFT_LOOPS':10, # was 15
                              'LOWNOISE_ASC':10,      # was 30 20190207, double gain to help suppress 0.47Hz osc. JCD 1Feb2019
                             },#close dsoft p
                          'Y':{'ENGAGE_SOFT_LOOPS_initial':2.5, #DSOFT Y is not used, but there is an if statement and a flag
                               'ENGAGE_SOFT_LOOPS_final':4, #probably unnecessary to make such a small gain adjustment
                              },#close dsoft y
                            },#close dsoft
                'MICH':{'P':{'ENGAGE_DRMI_ASC':-0.25,
                             'ENGAGE_PRMI_ASC':-0.2,
                            },
                        'Y':{'ENGAGE_DRMI_ASC':-0.2,
                             'ENGAGE_PRMI_ASC':-0.2,
                            },
                        },#close MICH
                'RPC':{'DHARD_P':{'14':0,
                              '19':-0.4,
                              '22.5':-0.8,
                              '24.5':-0.9,
                              '27.2':-1,
                              '29.5':-1.1,
                              '33':-1.2,
                              '36.5':-1.45,
                              '40':-1.6,
                            }, #close DHARD RPC P
                          'DHARD_Y':{'14':0,
                              '19':-0.4,
                              '22.5':-0.8,
                              '24.5':-0.9,
                              '27.2':-1,
                              '29.5':-1.1,
                              '33':-1.2,
                              '36.5':-1.45,
                              '40':-1.6,
                            }, #close RPC DHARD Y
                       'CHARD_P':{'14':0,
                                  '19':0.4,
                                  '22.5':0.8,
                                  '24.5':0.9,
                                  '27.2':1,
                                  '29.5':1.1,
                                  '33':1.2,
                                  '36.5':1.45,
                                  '40':1.6,
                                }, #close RPC CHARD P
                         'CHARD_Y':{'14':0,
                                  '19':0.6,
                                  '22.5':1.2,
                                  '24.5':1.3,
                                  '27.2':1.3,
                                  '29.5':1.6,
                                  '33':1.8,
                                  '36.5':2,
                                  '40':2.3,
                                }, #close  CHARD Y RPC
	                       'CSOFT_P':{'14':0,
	                                  '19':0.5,
	                                  '22.5':1,
	                                  '24.5':1.2,
	                                  '27.2':1.3,
	                                  '29.5':1.5,
	                                  '33':1.8,
	                                  '36.5':2.2,
	                                  '40':2.4,
	                                }, #close csoft RPC P
	                       'DSOFT_P':{'14':0,
	                                  '19':0.5,
	                                  '22.5':1,
	                                  '24.5':1.2,
	                                  '27.2':1.3,
	                                  '29.5':1.5,
	                                  '33':1.8,
	                                  '36.5':2.2,
	                                  '40':2.4,
	                                }, #close dsoft RPC P
                        },# close RPC
            }#close ASC gains

# filters to use when acquiring
filtmods = {'MICH':   ['FM5'],
            'PRCL':   ['FM4', 'FM9', 'FM10'],
            'SRCL':   ['FM9', 'FM10'],
            'ARM_IR': ['FM3', 'FM4', 'FM6'],
            'PRM_M2': ['FM1', 'FM3', 'FM4', 'FM6', 'FM10'],
            'SRM_M1': ['FM2', 'FM4', 'FM6', 'FM8', 'FM10'],
            }

sus_crossover = {'PRM_M1': -0.02,
                 'PRM_M2': 1,
                 'SRM_M1': -0.02} # changed from -0.02, which produced too much ringing in M1_LF


# DC readout parameters
dc_readout ={
    'DCPD_SUM_TARG': 20, # Used for acquisition, until at full power
    'DCPD_SUM_NLN': 40,  # 40mA is about 14pm with 38W injected, JCD SED 20Feb2020
    'PREF':          1.6868,
    'ERR_GAIN':     -2.9648e-6,
    'x0':            42*.73714,
    'sign':          1,
}


##############################
#### A2L gains for spot position setting
##############################
# To go from center to full power, need to walk around the ITMY absorber
#   Center -> AroundA -> AroundB -> FullPower
a2l_gains={'FULL_POWER':{
            'P2L':{'ITMX':-3.965,
                   'ITMY':-3,
                   'ETMX':4.3,
                   'ETMY':4.3},
            'Y2L':{'ITMX':0.055,
                   'ITMY':0.0,
                   'ETMX':3.6,
                   'ETMY':3.6},# July 2019 positions
                    },
        'AROUND_ABSORBER_A':{
            'P2L':{'ITMX':-3.0,
                   'ITMY':-2.0,
                   'ETMX':3.3,
                   'ETMY':1.0},
            'Y2L':{'ITMX':0,
                   'ITMY':3.2,
                   'ETMX':2.8,
                   'ETMY':4.4}, # spots that are far to the yaw side of the ITMY point absorber, to use on the way to full power July positions
		    },
        'AROUND_ABSORBER_B':{
            'P2L':{'ITMX':-4.0,
                   'ITMY':-3.0,
                   'ETMX':3.3,
                   'ETMY':1.0},
            'Y2L':{'ITMX':0,
                   'ITMY':3.2,
                   'ETMX':2.8,
                   'ETMY':4.4}, # spots that are far to the yaw side of the ITMY point absorber, to use on the way to full power July positions
		    },
        'CENTER':{
            'P2L':{'ITMX':0.85,
                   'ITMY':0.85,
                   'ETMX':0.85,
                   'ETMY':0.85},
            'Y2L':{'ITMX':0,
                   'ITMY':0,
                   'ETMX':0,
                   'ETMY':0},# Centered on the optic
                    }#spots that are the center of the optic
            }#close a2l gains dict

misalign_offsets = {'ETM': {'X': {'P': 10.15,
                                  'Y': -8.45},
                            'Y': {'P': 10.15,
                                  'Y': 8.45},
                            },#end ETMs
                    'TMS': {'X': {'P': -8.6,
                                  'Y': 31.6},
                            'Y': {'P': 40.1,
                                  'Y': 34.6}
                            }
                    }#end misalignment test offsets



# O2 values
#dc_readout ={
#    'DCPD_SUM_TARG': 20,
#    'PREF':          1.3689,
#    'ERR_GAIN':     -8.7614e-7,
#    'x0':            15.233,
#    'sign':          1,
#}


# trigger thresholds  # for several changes for DRMI see alog 44348
thresh = {'SRXY':                {'ON':0.3,  'OFF':0.2}, # JCD 29Jan2020. Was 0.4 and 0.3, but SRY just barely makes it above 0.3 sometimes.
          'ARM_IR':              {'ON':0.2,  'OFF':0.05},
          'ARM_IR_FMs':          {'ON':0.25, 'OFF':0.2},
          'DRMI_MICH_noarms':    {'ON':20,   'OFF':5},
          'DRMI_MICH_als':       {'ON':37,   'OFF':18},
          'DRMI_MICH_FMs':       {'ON':37,   'OFF':18},
          'DRMI_MICH_FMs_noarms':{'ON':24,   'OFF':5}, #added TVo DDB GLM 20181125
          'PRMIsb_MICH':         {'ON':8,   'OFF':5}, # Old Values 8 and 5. Lowered for low power PRMI locking, CRC July 23, 2018
          'PRMIcar_MICH':        {'ON':100,  'OFF':5},
          'DRMI_PRCL':           {'ON':-100, 'OFF':-100},
          'DRMI_PRCL_FMs':       {'ON':3.5,  'OFF':1},
          'DRMI_SRCL_noarms':    {'ON':10,   'OFF':5}, # Jul 26 2018, was 10, 5, Aug 5 2018, was 1, 2,
          'DRMI_SRCL_noarms_FMs':{'ON':5.5,  'OFF':1},
          'DRMI_SRCL_als':       {'ON':37,   'OFF':18},# 20180926: was 30,15 # Jul 26 2018, was 20, 10, Aug 5 2018, was 10, 1, Aug 22 2018, was 20, 15, 25
          'DRMI_SRCL_fromPRMI':  {'ON':48,  'OFF':5}, # 20180926: was 40,15 # Aug 22 2018, was 35, 15, 25
          'DRMI_SRCL_als_FMs':   {'ON':5.5,  'OFF':1}, #{'ON':40,  'OFF':18},#20181211 Dan+Rana # 20180926: was 40,15  # Aug 5 2018, was 5.5, 1, Aug 22 2018, was 20, 15, 25
          'DRMI_SRCL_noarms_FMs':{'ON':10,  'OFF':5}, # Aug 5 2018, was 5.5, 1
          'DRMI_ASC':            {'ON':0.001, 'CAUTION':0.001}, # Need to be updated for AS_C_NSUM
          'DARM':                {'ON': -100,  'OFF': -100},
          'ARMs_Green':          {'QUIET': 0.01, 'LOCKED': 0.8}, # LOCKED was 0.85; 08/19/2018
          'Override':            {'ON': -10000, 'OFF': -10000},
          'LOCKLOSS_CHECK_DRMI': {'ON': 25, 'OFF': 23},    # CRC created for IFO lockloss trigger in OFFLOAD_DRMI_ASC 
          'LOCKLOSS_CHECK':      {'ON': 700, 'OFF': 650},  # CRC, GLM, SED, HC changed from 110 and 108 to 800 and 750, and back to POP A DC OUT in ISC_LOCK #POPAIR_A_DC Jenne, Adrian, 26Sept2019
          'MICHDARK':            {'LOCKED': 2000},
          'MICHBRIGHT':          {'LOCKED': 5000}, #CHANGED FROM 1000 GLM DDB 20181123
          'PRXY':                {'LOCKED': 1, 'OSCILLATING': 6}, # Locked thresh to 0.5 from 1.0, for bad PRM alignment. JCD 6Aug2019, alog 51077
          'SRMI_MICH':           {'ON':1000, 'OFF':400},
          'SRMI_SRCL':           {'ON':-1000, 'OFF':-1000},
         }

# trigger masks
trig_mask = {'MICH':[2],
             'PRCL':[2],
             'SRCL':[]}

trigDelay = {'PRCL_FM':0.2,
             'MICH_FM':0.1,
             'SRCL_FM':0.25}

#FMtrigDelaySRCL = 0.25 # was 0.25 before 20180922

lsc_mtrx_elems = {'MICH':4,
                  'PRCL':3.5, # was 3.5 (GV 2018-09-27),
                  'SRCL':4}

offset = {'SRCL_MODEHOP':-800,
          'SRCL_RETUNE':50  # Running O3B Start with 50 ct. See LHO aLOG 52861. JSK / SED 
         }



# PD gain settings during acquisition
#SED wants to remove LSC REFL9 from this so that REFL A +B can be in the same units
pd_gain = {
    'LSC': {
        'REFLAIR': {
            '9':  0.25,
            '45': 1,
        }, # close PD
        'POPAIR': {
            '9':  1,
            '18': 2,
            '45': 1,
            '90': 4,
        }, # close PD
        'REFL': {
            '45': 0.275,
        }, # close PD
        'POP': {
            '9':  8,
            '45': 1,
        }, # close PD
    }, # close LSC
    'ASC': {
        'REFL': {
            '9':  1,
            '45': 1,
        }, # close PD
        'POP': {
            'X': 2.8,
        }, # close PD
        'AS': {
            '36': 2.8,
            '45': 1,
        }, # close PD
    }, # close ASC
} # close full dict


power_up_on_RF = False
use_reflair = False
reflair_relative_gain = 23  # we need 23dB more alanog gain if we are using REFL air than using REFL (lho alog 44378), and the same polarity

# AS Camera nominal exposure
camera_exp = {'ASAIR': 10000}

# VIOLIN damping settings
# list of filter modules to turn on, and gain, and drive dof (pit=default)
vio_settings = {
    # open ITMX
    'ITMX': {
#        '1': {
#            'FMs':['FM1', 'FM3', 'FM4'],
#            'gain': 0.5,
#            'drive': 'Y',
#            'max_gain': 2, # didn't ring up but didn't damp after 20 minutes at 4 on Y
#        }, #close mode    #This remains commented out because we are now damping it along with mode 9, using mode 9's filter.
        '2': {
            'FMs':  ['FM5','FM9','FM10'],
            'gain': -15, # nomial was -25.0, now -15, 05 aug 2020, rkumar, CV
            'drive': ['P'],
            'max_gain': -15,# MAX gain =  -15 RAHUL, CV, 10Aug2020
        }, # close mode       # Mode2 adjusted, 11Mar2019, Kara and Jenne.
        '3': {
            'FMs':  ['FM1', 'FM8', 'FM10'],# 
            'gain': -40,#nominal gain =  -40 RAHUL 17 MARCH 2020
            'drive': ['P'],
            'max_gain': -40,#MAX gain =  -40 RAHUL 17 MARCH 2020
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM2','FM4'],
            'gain': 0,
            'drive': ['Y'],
            'max_gain': 0,
        }, # close mode       # Mode4 adjusted, 11Mar2019, Kara and Jenne.
        '5': {
            'FMs':  ['FM1', 'FM2', 'FM4'],
            'gain': -5,
            'drive': ['P'],
            'max_gain': -5,
        }, # close mode
        '7': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 10,
            'drive': ['P'],
            'max_gain': 10,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM2','FM4'],
            'gain': -20,#was -4, now -20 (CV, 22dec19);was +10, damping filter change caused sign change, gain now -4 (CV, 20dec19)
            'drive': ['P'],
            'max_gain': -20,#was -4, now -20 (CV, 22dec19);was +10, damping filter change caused sign change, gain now -4 (CV, 20dec19)
        }, # close mode
         '9': {
             'FMs':  ['FM4','FM6'],
             'gain': 1, #was 0.4, rung up mode 1 GLM SED 20181130 (ITM RH change?)
             'drive': ['Y'],
             'max_gain': 3,
         }, # close mode
        '11': {# 995.177
            'FMs':  ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 600, #max gain could be 800
            'drive': ['P'],
            'max_gain': 600,
        }, # close mode
        '12': {# 995.462
            'FMs':  ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 200, #max gain could be 300
            'drive': ['P'],
            'max_gain': 200,
        }, # close mode
        '13': {# 998.083
            'FMs':  ['FM1', 'FM2', 'FM4', 'FM10'], # 270 deg
            'gain':20,# was set to 0, gain=+20 has consistantly worked (CV), old note: "was 100 but rang up mode 19"
            'drive': ['P'],
            'max_gain': 20,#was 30, guardian set to 30, it worked, but was not tested, setting to 20 (CV,3dec2019 )
        }, # close mode
        '14': {# 1001.843
            'FMs':  ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
            'gain': 500,
            'drive': ['P'],
            'max_gain': 500,
        }, # close mode
        '15': {# 1001.940
            'FMs':  ['FM1', 'FM2', 'FM6', 'FM10'], # 150 deg
            'gain': 400,
            'drive': ['P'],
            'max_gain': 400,
        }, # close mode
        '16': {# 1002.859
            'FMs':  ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 300,
            'drive': ['P'],
            'max_gain': 300,
        }, # close mode
        '17': {# 1003.120
            'FMs':  ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 120, #was 20, now 120, CV, RK, 11Jan2020 - could go much higher
            'drive': ['P'],
            'max_gain': 120,#was 20, now 120, CV, RK, 11Jan2020
        }, # close mode
        '19': { # 998.018  #this should be functional, but still needs testing
            'FMs': ['FM1', 'FM3', 'FM5', 'FM6', 'FM10'], # 270deg
            'gain': 20,#was 0, now 20, CV, RK, 11Jan2020
            'drive': ['Y'],
            'max_gain': 20,#was 0, now 20, CV, RK, 11Jan2020
        }, # close mode
    }, # close ITMX

    # open ITMY
    'ITMY': {
        '1': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -50,
            'drive': ['P'],
            'max_gain': -50,
        }, # close mode
        '2': {
            'FMs':  ['FM1','FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM5', 'FM10'], # old values before filter changes['FM1', 'FM3', 'FM4'], 10sep2020, CV
            'gain': 35, # new damping gain is positive 35, 10sep2020, CV, # was updated to be '0' gain 9sep2020, new freq. CV
            'drive': ['Y'], # was ['P'] now ['Y'], 10sep2020, CV
            'max_gain': 35, #was updated 9sep2020, new freq. CV: changed from 0, RKumar 20March2020
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM3', 'FM4'], # new filter as of 11sep2020, CV #this is the tricky doublet
            'gain': -10,# new filter damps with -10 gain, old gain was -3, CV # sign flip July 30 2020  #sign flipped back Aug 2
            'drive': ['P'],
            'max_gain': -10, # new max gain = -10, old max gain was -3, CV
        }, # close mode
        '5': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -20,
            'drive': ['P'],
            'max_gain': -20,
        }, # close mode
        '6': {
            'FMs':  ['FM1','FM3','FM4'],
            'gain': 2,
            'drive': ['Y'],
            'max_gain': 2,
        }, # close mode
        '7': {
            'FMs':  ['FM1','FM2','FM4','FM7'], #  11sep2020, FM7 added, CV 
            'gain': 0.5, # new gain is 0.5, old gain 0.1, CV
            'drive': ['Y'],
            'max_gain': 0.5, # new gain is 0.5, old gain 0.2, CV
        }, # close mode
        '8': {
            'FMs':  ['FM1','FM4'],#before pre-loading this was FM1,3,4, negaitve gain, after no phase, +gain
            'gain': 4,
            'drive': ['Y'],
            'max_gain': 4,
        }, # close mode
        '9': {
            'FMs':  ['FM1', 'FM4'],#sign flip July 30 2020 #flipped back Aug 2nd
            'gain': -5,
            'drive': ['P'],
            'max_gain': -5,
        }, # close mode
        '11': { # 991.641
            'FMs': ['FM1', 'FM2', 'FM10'], # 330 deg
            'gain': 10, #03b nominal was 350 and max 500 Rkumar 03 Aug 2020
            'drive': ['P'],
            'max_gain': 35,
        }, # close mode
        '12': { # 991.828
            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 120,
            'drive': ['P'],
            'max_gain': 120,#changed from 0, RKumar 20March2020
        }, # close mode
        '13': { # 994.801
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 35, #max gain could be 500 #sign flip July 30 2020 #flip back July 30 2020 #reduced gain to +35 on 03 aug 2020 rkumar
            'drive': ['P'],
            'max_gain': 35,
        }, # close mode
        '14': { # 995.053
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 50,
            'drive': ['P'],
            'max_gain': 50,
        }, # close mode
        '15': { # 997.617 #This needs to be tested
            'FMs': ['FM1', 'FM5', 'FM10'], # 90 deg
            'gain': 50, #reduced gain to +50 from 500, on 03 aug 2020 rkumar
            'drive': ['P'],
            'max_gain': 50, #changed from 0, RKumar 20March2020 #reduced gain to +50 from 500, on 03 aug 2020 rkumar
        }, # close mode
        '16': { # 997.785
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 5, #could go a bit higher #reduced gain to +5 from +50, on 03 aug 2020 rkumar
            'drive': ['P'],
            'max_gain': 50,
        }, # close mode
        '17': { # 998.967 # this is being damped, new filter 11sep2020,CV
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 30, # 11sep2020, new filter gain = 30, old filter was 9, CV #reduced gain to +9 from 90, on 03 aug 2020 rkumar
            'drive': ['P'],
            'max_gain': 30,# 11sep2020, new filter gain = 30, old filter was 90, CV 
        }, # close mode
#        '18': { # 998.825 # this is being damped in guardian for the firs time with a new filter as of 11sep2020,CV
#            'FMs': ['FM1', 'FM2', 'FM10'], # 180 deg
#            'gain': 30, # 11sep2020, new filter gain = 30, CV
#            'drive': ['P'],
#            'max_gain': 30,# 11sep2020, new filter gain = 30 CV 
#        }, # close mode
    }, # close ITMY

    # open ETMX
    'ETMX': {
        '2': {
            'FMs':  ['FM1','FM2','FM4'],
            'gain': -0.2,
            'drive': ['Y'],
            'max_gain': -20,
        }, # close mode
        '3': {
            'FMs':  ['FM1', 'FM3','FM4'],
            'gain': 15,
            'drive': ['P'],
            'max_gain': 15,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 20,
            'drive': ['P'],
            'max_gain': 20,
        }, # close mode
        '6': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
        }, # close mode
        '7': {
            'FMs':  ['FM1','FM2', 'FM4'],
            'gain': 3,
            'drive': ['Y'],
            'max_gain': 3,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM2', 'FM4'],
            'gain': -10,
            'drive': ['P'],
            'max_gain': -10,
        }, # close mode
        '9': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -10,# was -3, now -10, CV, RK, 11Jan2020
            'drive': ['Y'],
            'max_gain': -10,# was -3, now -10, CV, RK, 11Jan2020
        }, # close mode


        '11': { # 1006.439
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 30,
            'drive': ['P'],
            'max_gain': 30,
        }, # close mode
        '12': { # 1006.747
            'FMs': ['FM1', 'FM10'], # 0 deg
            'gain': 100,
            'drive': ['P'],
            'max_gain': 100,
        }, # close mode
        '13': { # 1010.356
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 30,
            'drive': ['P'],
            'max_gain': 30,
        }, # close mode
        '14': { # 1010.581
            'FMs': ['FM1', 'FM3', 'FM5', 'FM10'], # 90 deg
            'gain': 500,
            'drive': ['P'],
            'max_gain': 500,
        }, # close mode
        '15': { # 1011.063
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 25,
            'drive': ['P'],
            'max_gain': 25,
        }, # close mode
        '16': { # 1013.869
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 30,
            'drive': ['P'],
            'max_gain': 30,
        }, # close mode
        '17': { # 1014.091
            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 200, #could go up to 300
            'drive': ['P'],
            'max_gain': 200,
        }, # close mode
#        '18': { # 1011.336  #commented out for more testing
#            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
#            'gain': 2000, #could go up to 300
#            'drive': ['P'],
#            'max_gain': 2000,
#        }, # close mode
    }, # close ETMX

    # open ETMY
    'ETMY': {
        '1': {
            'FMs':  ['FM1','FM4','FM8'],  # removed FM7 (-60deg) added FM8 (+60deg) 2Sep2020, tested +60deg phase @ 0.1 gain on relocking, it road through the violins being turned off and on, and continued to damp, putting in guardian, CV, rkumar. OLD NOTES: AUgust 4th 2020, phase changed to -60 gain positive, before was negative gain phase +60 #changed the phase to +60 and gain +0.6, 05 aug 2020 rkumar  #this mode keeps spining! SED damped with positive gain +60 deg August 9 2020. Not putting in guardian
            'gain': 0.1, # set gain to 0.1, 2Sep2020, to use with +60deg phase, CV, rkumar.  OLD NOTES: #sign flip July 30 2020 # nominal was -0.1 with phase +60, now set to +0.4, 05 aug 2020 rkumar, CV
            'drive': ['Y'],
            'max_gain': 0.1, # keeping max gain of 0.1 for new phase of +60deg, CV, rkumar, 2Sep2020.  OLD NOTES: max gain set to 0.4, 10Aug2020, rkumar, CV, gain=2 rang up, damping on yaw (unknown date)
        }, # close mode
        '2': {
            'FMs':  ['FM1','FM3','FM4'],
            'gain': -2,
            'drive': ['Y'],
            'max_gain': -2,
        }, # close mode
        '3': {
            'FMs':  ['FM1','FM3', 'FM4'],
            'gain': 5,
            'drive': ['Y'],
            'max_gain': 5,
        }, # close mode
        '4': {
            'FMs':  ['FM1', 'FM4'],
            'gain': -3.0,
            'drive': ['Y'],
            'max_gain': -3,
        }, # close mode
        '5': {
            'FMs':  ['FM1', 'FM4'],
            'gain': 0.2,
            'drive': ['Y'],
            'max_gain': 2,
        }, # close mode
        '6': {
            'FMs':  ['FM1', 'FM3','FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,# was -4, but never turned on, setting to 0 until tested (CV 14dec19)
        }, # close mode
        '7': {
            'FMs':  ['FM1', 'FM10'], # moved 100dB from FM4 to FM10, CV, 13aug2020
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '8': {
            'FMs':  ['FM1', 'FM10'], # moved 100dB from FM4 to FM10, CV, 13aug2020
            'gain': -2,
            'drive': ['Y'],
            'max_gain': -2,
        }, # close mode
        '9': {
            'FMs':  ['FM2', 'FM4'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '10': {
            'FMs':  ['FM2', 'FM4', 'FM5'],
            'gain': 0,
            'drive': ['P'],
            'max_gain': 0,
        }, # close mode
        '11': { # 1000.061
            'FMs': ['FM1', 'FM6', 'FM10'], # 180 deg
            'gain': 20, # set to 20, from 50, 10Aug2020, CV, rkumar
            'drive': ['P'],
            'max_gain': 20, # set max gain to 20, from 50, 10Aug2020, CV, rkumar
        }, # close mode
        '12': { # 1000.423, new damping filter in FM7, as of 6Aug2020, CV, rkumar
            'FMs': ['FM7', 'FM10'], # 0 deg, changed filter from FM1 to FM7, 10Aug2020, CV, rkumar
            'gain': 6, # set gain to 6, from 0.5, 10Aug2020, CV, rkumar
            'drive': ['P'],
            'max_gain': 6, # set gain to 6, from 0.5, 10Aug2020, CV, rkumar
        }, # close mode
        '13': { # 1009.932
            'FMs': ['FM1', 'FM4', 'FM10'], # 300 deg
            'gain': 50, # set gain to 50, from 130, CV, rkumar, 10Aug2020
            'drive': ['P'],
            'max_gain': 50, # set max gain to 50, from 130, CV, rkumar, 10Aug2020
        }, # close mode
        '14': { # 1017.985
            'FMs': ['FM1', 'FM2', 'FM6', 'FM10'], # 150 deg
            'gain': 20, #increased to 20, CV, rkumar, 10Aug2020. reduced the gain to 10% of nominal (100), 05 aug 2020, rkumar, CV
            'drive': ['P'],
            'max_gain': 20, #increased to 20, CV, rkumar, 10Aug2020.  set max gain to 10, from 100, CV, rkumar, 10Aug2020
        }, # close mode
        '15': { # 1018.238
            'FMs': ['FM1', 'FM4', 'FM6', 'FM10'], # 120 deg
            'gain': 150,  #reduced the gain to 10% of nominal (1500), 05 aug 2020, rkumar, CV
            'drive': ['P'],
            'max_gain': 150, # set max gain to 150, from 1500, CV, rkumar, 10Aug2020
        }, # close mode
        '18': { # 1000.294
            'FMs': ['FM1', 'FM4', 'FM10'], #added a phase of -60 degrees, rkumar, CV, 06 aug 2020, removed FM6, CV, 10Aug2020
            'gain': 0,# gain set to 0, from 6, CV, rkumar, 10Aug2020
            'drive': ['P'],
            'max_gain': 0, # gain set to 0, from 6, CV, rkumar, 10Aug2020
        }, # close mode
	    '20': { # 1000.307, old notes prior to 10Aug2020 removed
            'FMs': ['FM1', 'FM4', 'FM10'], # removed FM6, CV, 10Aug2020
            'gain': 0,  # returned gain to 0, still testing, CV, rkumar, 10Aug2020. gain set to 2, from 0, CV, rkumar, 10Aug2020
            'drive': ['Y'],
            'max_gain': 0,# returned gain to 0, still testing, CV, rkumar, 10Aug2020. gain set to 2, from 0, CV, rkumar, 10Aug2020
        }, # close mode
    }, # close ETMY
} # close dict

vio_mon = {  #monitor levels for which the guardian should do certain things
    'RF_noise_floor':3.0, # this is the noise floor when
    'DC_noise_floor':1.0,
    'Rung_up':5.5,# this mode is rung up so higfh we should engage with low gain
    'Nominal':5}
