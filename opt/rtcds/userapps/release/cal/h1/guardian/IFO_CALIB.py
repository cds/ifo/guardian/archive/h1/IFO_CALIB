from guardian import GuardState, GuardStateDecorator
import os
import time, datetime, tempfile, shutil
from subprocess import Popen, PIPE, call
import string
#################################################
request = 'IDLE'
nominal = request 

# Variables
import lscparams # load configuration file holding calirbation line amplitudes
runMode = 1 # 0 for just reading the file and 1 for taking new measurement
currentDate = str(datetime.date.today())
mainRunCode = '/opt/rtcds/userapps/release/cal/l1/guardian/dttRunOrExtract.py'
outPrefix = '/ligo/svncommon/CalSVN/aligocalibration/trunk/Runs/O3/L1/Measurements'
element=list(string.ascii_lowercase)

#################################################
# Paths to injection templates
#################################################
pcalTemplate = outPrefix + '/templates/PCAL_PCALY2DARMTF_5to1000Hz_lower_amplitudes_template.xml'
olgTemplate = outPrefix + '/templates/DARM_OLGTF_5to1000Hz_quick_template.xml'
esdy2darmTemplate = outPrefix + '/templates/L1SUSETMY_iEXC2DARMTF_5to1000Hz_L3_quick_template.xml'
pumx2darmTemplate = outPrefix + '/templates/L1SUSETMX_iEXC2DARMTF_5to1000Hz_L2_quick_template.xml'
uimx2darmTemplate = outPrefix + '/templates/L1SUSETMX_iEXC2DARMTF_5to100Hz_L1_quick_template.xml'


#################################################
# Paths to injection test templates
#################################################
#pcalTemplate = outPrefix + '/templates/PCAL_template_new_cal_line_quick_TMTEST.xml' # For testing purposes
#olgTemplate = pcalTemplate#outPrefix + '/templates/DARM_OLGTF_5to1000Hz_quick_template.xml'
#esdy2darmTemplate = pcalTemplate#outPrefix + '/templates/L1SUSETMY_iEXC2DARMTF_5to1000Hz_L3_quick_template.xml'
#pumx2darmTemplate = pcalTemplate#outPrefix + '/templates/L1SUSETMX_iEXC2DARMTF_5to1000Hz_L2_quick_template.xml'
#uimx2darmTemplate = pcalTemplate#outPrefix + '/templates/L1SUSETMX_iEXC2DARMTF_5to100Hz_L1_quick_template.xml'

## DEFINITIONS
# Check if IFO is in low noise
def IFO_in_lownoise():
    return ezca['GRD-ISC_LOCK_STATE_N'] >= 2000

# Clear excitations on PCALY
def CLEAR_PCALY_EXC():
    cmdFile = tempfile.NamedTemporaryFile(delete=False)
    cmdFile.write('tp clear 125.*\n') # clear excitations; number specific to PCALY
    cmdFile.write('quit\n')
    cmdFile.close()
    cmdPath = cmdFile.name
    p = Popen(['diag', '-g', 'fft', '-x', '-l', '-f', cmdPath],stdout=PIPE,stderr=PIPE)
    while p.poll() is None:
        self.timer['wait'] = 0.5
    return True

# Clear excitations on PCALX
def CLEAR_SUSY_EXC():
    cmdFile = tempfile.NamedTemporaryFile(delete=False)
    cmdFile.write('tp clear 97.*\n') # clear excitations; number specific to SUSY
    cmdFile.write('quit\n')
    cmdFile.close()
    cmdPath = cmdFile.name
    p = Popen(['diag', '-g', 'fft', '-x', '-l', '-f', cmdPath],stdout=PIPE,stderr=PIPE)
    while p.poll() is None:
        self.timer['wait'] = 0.5
    return True


## DECORATORS
# To check the state IFO before each sweep
class assert_ifo_in_lownoise(GuardStateDecorator):
    def pre_exec(self):
        if not IFO_in_lownoise():
            log('IFO lost lock!')
            return 'IDLE'

##################################################
class INIT(GuardState):
    index = 0
    request = True
    goto = True   
 
    def main(self):
        return True

#################################################
# Calibration Failed -- Restore IFO settings
#################################################
class IFO_DOWN(GuardState):
    index = 2
    request = True
    goto = True

    def main(self):
        # set PCal lines amplitudes to their original values
        # TRAMP of 10 sec is already set
        ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
        self.timer['wait'] = 1.0
        ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
        self.timer['wait'] = 1.0
        ezca['CAL-PCALY_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
        self.timer['wait'] = 1.0
        
        # set SUS L3 line amplitude back to its original value
        ezca['SUS-ETMY_L3_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line3_clkgain      
        self.timer['wait'] = 1.0

        # set SUS L2 line amplitude back to its original value
        ezca['SUS-ETMX_L2_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line2_clkgain      
        self.timer['wait'] = 1.0

        # set SUS L1 line amplitude back to its original value
        ezca['SUS-ETMX_L1_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line1_clkgain      
        self.timer['wait'] = 1.0

        # set ASC ADS to value before taking sweeps
        #ezca['ASC-ADS_GAIN'] = ascads_sp
        ezca['ASC-ADS_GAIN'] = 0.07 # See Note 1
		
		# Note 1:
		# This value is being hard coded becuase it is not possible to assign values 
		# to variables and carry it though multiple states in the guadian infractructure.
		# Maybe we can add this inoto the ifoconfig.py file. The reason this has not been
		# done already is due the gain value sometimes changes. Therefore, if the gain
		# were to change then one must also remember to update the ifoconfig.py file - or
		# in this case, remember to update this value.  
	
        # close PCAL swept sine path
        ezca['CAL-PCALY_SWEPT_SINE_ON'] = 0

        self.timer['wait'] = 15.0 # wait for the ramps
        notify('Restored IFO Settings')
        return True

    def run(self):
        notify('Calibration Sweep FAILED -- see log for details')
        return True

###############################################
class IDLE(GuardState):
    index = 1
    goto = True

    def run(self):
        if IFO_in_lownoise():
            return True
        else:
            notify('IFO is not in low noise state')

##################################################
# Prepare IFO for calibration sweeps
##################################################
class GETTING_READY(GuardState):
    index = 5
    request = True

    @assert_ifo_in_lownoise
    def main(self):
        if runMode:
            notify('Preparing for calibration sweeps')
            
            currentDate = str(datetime.date.today())
            
            # Make directories in the Cal svn to save the PCAL measurements
            for el,nel in zip(element,element[1:]):
                notify(el)
                if not os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
                    os.mkdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                    break                
                elif len(os.listdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))) != 0:
                    if not os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel)):
                        os.mkdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
                        break
                        
            # Make directories in the Cal svn to save the ACTUATION measurements 
            for el,nel in zip(element,element[1:]):
                notify(el)
                if not os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
                    os.mkdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                    break                
                elif len(os.listdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))) != 0:
                    if not os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel)):
                        os.mkdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
                        break
                                    
            # Make directories in the Cal svn to save the SENSING measurements
            for el,nel in zip(element,element[1:]):
                notify(el)
                if not os.path.isdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el)):
                    os.mkdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el))
                    break                
                elif len(os.listdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el))) != 0:
                    if not os.path.isdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + nel)):
                        os.mkdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + nel))
                        break
                        
            # Old scheme                                                                  
                #if not os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
                    #os.mkdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                #if not os.path.isdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el)):
                #    os.mkdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el))
                #    break 

            ## Turn off all calibration lines
            # ASC ADS gain
            #ascads_sp = 0.1 #ezca['ASC-ADS_GAIN']
            ezca['ASC-ADS_GAIN'] = 0
            self.timer['wait'] = 0.2

            # SUS LINE L1 (ETMX)
            ezca['SUS-ETMX_L1_CAL_LINE_TRAMP'] = 10
            self.timer['wait'] = 0.2
            ezca['SUS-ETMX_L1_CAL_LINE_CLKGAIN'] = 0

            # SUS LINE L2 (ETMX)
            ezca['SUS-ETMX_L2_CAL_LINE_TRAMP'] = 10
            self.timer['wait'] = 0.2
            ezca['SUS-ETMX_L2_CAL_LINE_CLKGAIN'] = 0 

            # SUS LINE L3 (ETMY)
            ezca['SUS-ETMY_L3_CAL_LINE_TRAMP'] = 10
            self.timer['wait'] = 0.2
            ezca['SUS-ETMY_L3_CAL_LINE_CLKGAIN'] = 0
        
            # PCAL LINE 1
            ezca['CAL-PCALY_PCALOSC1_OSC_TRAMP'] = 10
            self.timer['wait'] = 0.2
            ezca['CAL-PCALY_PCALOSC1_OSC_SINGAIN'] = 0

            # PCAL LINE 2
            ezca['CAL-PCALY_PCALOSC2_OSC_TRAMP'] = 10
            self.timer['wait'] = 0.2
            ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = 0

            # PCAL LINE 3
            ezca['CAL-PCALY_PCALOSC3_OSC_TRAMP'] = 10
            self.timer['wait'] = 0.2
            ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = 0

            ## open PCAL swept sine path
            ezca['CAL-PCALY_SWEPT_SINE_ON'] = 1

            self.timer['wait'] = 15.0 # wait for the ramps
        else:
            notify('Just reading the xml files')

    def run(self):
        return True

################################################
# PCAL Sensing for OLG
################################################
class PCAL_SWEEP_SENSING(GuardState):
    index = 10
    request = True


    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())
        
        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1_PCALY2DARMTF_5to1000Hz_OLG.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                    break
                    
        # Set up file names  
        self.SaveFile = outDir + '/' + currentDate +'_L1_PCALY2DARMTF_5to1000Hz_OLG.xml'	# define the location and file name of the post sweep DTT xml
        shutil.copy2(pcalTemplate, self.SaveFile)	# Copy the template file (line 19) to the location given in the previous line
        notify(pcalTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_PCAL2DARMTF_5to1000Hz_A_PCALYRX_B_DARMIN1_tf_OLG'	# define the location and the file name for data extraction of TFs & COH
        
        # Run the DTT xml
        self.process = Popen(['python', mainRunCode, '-x', self.SaveFile,'-o', self.outFile,'-r', str(runMode),'-d','1','-a','L1:CAL-PCALY_RX_PD_OUT','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE) # run the process
         
    def run(self):
        if self.process.poll() is None:
            notify('Sensing PCal sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()	# kill the sweep if the IFO drops out of low noise
                CLEAR_PCALY_EXC()	# clear the excitation channel
                notify('IFO Low Noise Fail -- PCAL OLG Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('Sensing PCal sweep done')
            self.status = call("chmod 666 " + self.SaveFile + " " + self.outFile,shell=True)
            return True

################################################
# DARM Open Loop Gain (DARM OLG)
################################################
class DARM_OLG_SWEEP_SENSING(GuardState):
    index = 15
    request = True
    
    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())
        
        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1_DARM_OLGTF_5to1000Hz.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el))
                    break
        
        notify(outDir)
        
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el)):
        #        outDir = (str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + el))
        #        if len(os.listdir(outDir)) != 0:
        #            outDir = (str(outPrefix + '/' + 'FullIFOSensingTFs' + '/' + currentDate + nel))
        #            break
                                   
        # Set up file names                 
        self.SaveFile = outDir + '/' + currentDate + '_L1_DARM_OLGTF_5to1000Hz.xml'
        shutil.copy2(olgTemplate, self.SaveFile)
        notify(olgTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_DARM_OLGTF_5to1000Hz_A_DARMIN2_B_DARMEXC_tf'
        self.outDir = outDir
                
        # Run the DTT xml
        self.process = Popen(['python', mainRunCode, '-x', self.SaveFile, '-o', self.outFile,'-r',str(runMode),'-d','1','-a','L1:LSC-DARM_IN2','-b','L1:LSC-DARM_EXC','-n','True'], stdout=PIPE, stderr=PIPE)

    def run(self):
        outDir = self.outDir
        if self.process.poll() is None:
            notify('Sensing OLG sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process1.kill()
                CLEAR_SUSY_EXC()
                notify('IFO Low Noise Fail -- DARM OLG Measurement Aborted')
                return 'IFO_DOWN'
        else:
            self.outFile = outDir + '/' + currentDate + '_L1_DARM_OLGTF_5to1000Hz_A_DARMIN2_B_DARMIN1_tf'
            process2 = Popen(['python', mainRunCode, '-x', self.SaveFile,'-o',self.outFile,'-r','0','-d','1','-a','L1:LSC-DARM_IN2','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE) # only read file
            self.status = call("chmod 666 " + self.outFile,shell=True) # Allow group users to read and write the file (since guarduian default user is controls).
            while process2.poll() is None:
                self.timer['wait'] = 1.0
            else:
                notify('Sensing OLG sweep done')
                self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
                return True

################################################
# PCAL sweep for L3
################################################
class PCAL_SWEEP_L3(GuardState):
    index = 20
    request = True

    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())
        
        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1_PCALY2DARMTF_5to1000Hz_L3.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                    break
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
        #        outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
        #        if len(os.listdir(outDir)) != 0:
        #            outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
        #            break
                    
        # Set up file names         
        self.SaveFile = outDir + '/' + currentDate + '_L1_PCALY2DARMTF_5to1000Hz_L3.xml'
        shutil.copy2(pcalTemplate, self.SaveFile)
        notify(pcalTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_PCAL2DARMTF_5to1000Hz_A_PCALYRX_B_DARMIN1_tf_L3'
        
        # Run the DTT xml
        self.process = Popen(['python', mainRunCode, '-x', self.SaveFile,'-o', self.outFile,'-r', str(runMode),'-d','1','-a','L1:CAL-PCALY_RX_PD_OUT','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE)

    def run(self):
        if self.process.poll() is None:
            notify('L3 acutation PCal sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()
                CLEAR_PCALY_EXC()
                notify('IFO Low Noise Fail -- PCAL L3 Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('L3 acutation PCal sweep done')
            self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
            return True

################################################
# L3 Actuation sweep
################################################
class ACTUATION_SWEEP_L3(GuardState):
    index = 25
    request = True
    
    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())

        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1SUSETMY_iEXC2DARMTF_5Hz_1000Hz_L3.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                    break
                            
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
        #        outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
        #        if len(os.listdir(outDir)) != 0:
        #            outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
        #            break
                    
        # Set up file names                    
        self.SaveFile = outDir + '/' + currentDate + '_L1SUSETMY_iEXC2DARMTF_5Hz_1000Hz_L3.xml'
        shutil.copy2(esdy2darmTemplate, self.SaveFile)
        notify(esdy2darmTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_ETMY_L3_5Hzto1000Hz_A_TEST_B_DARMIN1_tf'
        
        # Run the DTT xml
        self.process = Popen(['python', mainRunCode, '-x', self.SaveFile, '-o', self.outFile,'-r',str(runMode),'-d','1','-a','L1:SUS-ETMY_L3_TEST_L_EXC','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE)
        
    def run(self):
        if self.process.poll() is None:
            notify('L3 acutation sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()
                CLEAR_SUSY_EXC()
                notify('IFO Low Noise Fail -- Actuation L3 Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('L3 acutation sweep done')
            self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
            return True

################################################
# PCAL sweep for L2
################################################
class PCAL_SWEEP_L2(GuardState):
    index = 30
    request = True
    
    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())

        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1_PCALY2DARMTF_5to1000Hz_L2.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                    break
                    
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):L1SUSETMX_iEXC2DARMTF_5to1000Hz_L2_quick_template.xml
         #       outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
         #       if len(os.listdir(outDir)) != 0:
         #           outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
         #           break
                    
        # Set up file names                             
        self.SaveFile = outDir + '/' + currentDate + '_L1_PCALY2DARMTF_5to1000Hz_L2.xml'
        shutil.copy2(pcalTemplate, self.SaveFile)
        notify(pcalTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_PCAL2DARMTF_5to1000Hz_A_PCALYRX_B_DARMIN1_tf_L2'
        
        # Run the DTT xml
        self.process = Popen(['python',mainRunCode, '-x', self.SaveFile,'-o', self.outFile,'-r', str(runMode),'-d','1','-a','L1:CAL-PCALY_RX_PD_OUT','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE)

    def run(self):
        if self.process.poll() is None:
            notify('L2 acutation PCal sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()
                CLEAR_PCALY_EXC()
                notify('IFO Low Noise Fail -- PCAL L2 Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('L2 acutation PCal sweep done')
            self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
            return True

################################################
# L2 Actuation sweep
################################################
class ACTUATION_SWEEP_L2(GuardState):
    index = 35
    request = True
   
    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())
        
        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1SUSETMX_iEXC2DARMTF_5Hz_1000Hz_L2.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                    break        
        
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
        #        outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
        #        if len(os.listdir(outDir)) != 0:
        #            outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
        #            break
                             
        # Set up file names        
        #pumx2darmTemplate = outPrefix + '/templates/L1SUSETMX_iEXC2DARMTF_5to1000Hz_L2_quick_template.xml'
        self.SaveFile = outDir + '/' + currentDate + '_L1SUSETMX_iEXC2DARMTF_5Hz_1000Hz_L2.xml'
        shutil.copy2(pumx2darmTemplate, self.SaveFile)
        notify(pumx2darmTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_ETMX_L2_5Hzto1000Hz_A_TEST_B_DARMIN1_tf'
        
        # Run the DTT xml
        self.process = Popen(['python',mainRunCode, '-x', self.SaveFile, '-o', self.outFile,'-r',str(runMode),'-d','1','-a','L1:SUS-ETMX_L2_TEST_L_EXC','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE)

    def run(self):
        if self.process.poll() is None:
            notify('L2 acutation sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()
                CLEAR_SUSY_EXC()
                notify('IFO Low Noise Fail -- Actuation L2 Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('L2 acutation sweep done')
            self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
            return True

################################################
# PCAL sweep for L1
################################################
class PCAL_SWEEP_L1(GuardState):
    index = 40
    request = True
     
    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())
        
        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1_PCALY2DARMTF_5to1000Hz_L1.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
                    break        
        
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el)):
        #        outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + el))
        #        if len(os.listdir(outDir)) != 0:
        #            outDir = (str(outPrefix + '/' + 'PCAL' + '/' + currentDate + nel))
        #            break
                    
        # Set up file names  
        self.SaveFile = outDir + '/' + currentDate + '_L1_PCALY2DARMTF_5to1000Hz_L1.xml'
        shutil.copy2(pcalTemplate, self.SaveFile)
        notify(pcalTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_PCAL2DARMTF_5to1000Hz_A_PCALYRX_B_DARMIN1_tf_L1'
        
        # Run the DTT xml
        self.process = Popen(['python',mainRunCode, '-x', self.SaveFile,'-o', self.outFile,'-r', str(runMode),'-d','1','-a','L1:CAL-PCALY_RX_PD_OUT','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE)

    def run(self):
        if self.process.poll() is None:
            notify('L1 acutation PCal sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()
                CLEAR_PCALY_EXC()
                notify('IFO Low Noise Fail -- PCAL L1 Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('L1 acutation PCal sweep done')
            self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
            return True

################################################
# L1 Actuation sweep
################################################
class ACTUATION_SWEEP_L1(GuardState):
    index = 45
    request = True
    
    @assert_ifo_in_lownoise
    def main(self):
        currentDate = str(datetime.date.today())
        
        # Determine the path where to save the data
        for el,nel in zip(element,element[1:]):
            if os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
                outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                if os.path.isfile(outDir+ '/' + currentDate +'_L1SUSETMX_iEXC2DARMTF_5Hz_100Hz_L1.xml.xml') == 1:
                    outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
                    continue
                else:
                    outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
                    break 
                            
        #for el,nel in zip(element,element[1:]):
        #    if os.path.isdir(str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el)):
        #        outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + el))
        #        if len(os.listdir(outDir)) != 0:
        #            outDir = (str(outPrefix + '/' + 'FullIFOActuationTFs' + '/' + currentDate + nel))
        #            break
                    
        # Set up file names          
        #uimx2darmTemplate = outPrefix + '/templates/L1SUSETMX_iEXC2DARMTF_5to100Hz_L1_quick_template.xml'
        self.SaveFile = outDir + '/' + currentDate + '_L1SUSETMX_iEXC2DARMTF_5Hz_100Hz_L1.xml'
        shutil.copy2(uimx2darmTemplate, self.SaveFile)
        notify(uimx2darmTemplate)
        notify(self.SaveFile)
        self.outFile = outDir + '/' + currentDate + '_L1_ETMX_L1_5Hzto100Hz_A_TEST_B_DARMIN1_tf'
        
        # Run the DTT xml
        self.process = Popen(['python',mainRunCode, '-x', self.SaveFile, '-o', self.outFile,'-r',str(runMode),'-d','1','-a','L1:SUS-ETMX_L1_TEST_L_EXC','-b','L1:LSC-DARM_IN1','-n','True'], stdout=PIPE, stderr=PIPE)

    def run(self):
        if self.process.poll() is None:
            notify('L1 acutation sweep running')
            self.timer['wait'] = 5.0
            if not IFO_in_lownoise():
                self.process.kill()
                CLEAR_SUSY_EXC()
                notify('IFO Low Noise Fail -- Actuation L1 Measurement Aborted')
                return 'IFO_DOWN'
        else:
            notify('L1 acutation sweep done')
            self.status = call("chmod 666 " + self.outFile + " " + self.SaveFile,shell=True) # Allow group users to read and write the data extracted file and the .xml (since guarduian default user is controls).
            return True

#################################################
# Calibration Complete -- Restore IFO settings
#################################################
class COMPLETE_CALIBRATION(GuardState):
    index = 55
    request = True

    @assert_ifo_in_lownoise
    def main(self):
        # set PCal lines amplitudes to their original values
        # TRAMP of 10 sec is already set
        ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
        self.timer['wait'] = 1.0
        ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
        self.timer['wait'] = 1.0
        ezca['CAL-PCALY_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
        self.timer['wait'] = 1.0
        
        # set SUS L3 line amplitude back to its original value
        ezca['SUS-ETMY_L3_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line3_clkgain      
        self.timer['wait'] = 1.0

        # set SUS L2 line amplitude back to its original value
        ezca['SUS-ETMX_L2_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line2_clkgain      
        self.timer['wait'] = 1.0

        # set SUS L1 line amplitude back to its original value
        ezca['SUS-ETMX_L1_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line1_clkgain      
        self.timer['wait'] = 1.0

        # set ASC ADS to value before taking sweeps
        #ezca['ASC-ADS_GAIN'] = ascads_sp
        ezca['ASC-ADS_GAIN'] = 0.07
  
        # close PCAL swept sine path
        ezca['CAL-PCALY_SWEPT_SINE_ON'] = 0

        self.timer['wait'] = 15.0 # wait for the ramps

    def run(self):
        notify('Calibration sweeps done!')
        return True

###################################################
edges = [
    ('INIT','IDLE'),
    ('IDLE', 'GETTING_READY'),
    ('GETTING_READY','PCAL_SWEEP_SENSING'),
    ('PCAL_SWEEP_SENSING','DARM_OLG_SWEEP_SENSING'),
    ('DARM_OLG_SWEEP_SENSING','PCAL_SWEEP_L3'),
    ('PCAL_SWEEP_L3','ACTUATION_SWEEP_L3'),
    ('ACTUATION_SWEEP_L3','PCAL_SWEEP_L2'),
    ('PCAL_SWEEP_L2','ACTUATION_SWEEP_L2'),
    ('ACTUATION_SWEEP_L2','PCAL_SWEEP_L1'),
    ('PCAL_SWEEP_L1','ACTUATION_SWEEP_L1'),
    ('ACTUATION_SWEEP_L1','COMPLETE_CALIBRATION'),
    ('IFO_DOWN','IDLE')
    ]
